module MenusHelper
  def add_menu_with_categories(attributes)
    menu = Menu.new(name: attributes[:name], description: attributes[:description], price: attributes[:price])
    category_ids = attributes[:category_ids]
    categories = []
    category_ids.each do |category_id|
      categories << Category.find(category_id)
    end
    categories.each do |category|
      menu.menu_categories << MenuCategory.create(category: category)
    end
    menu
  end

  def update_menu_with_category(attributes, menu)
    menu.menu_categories.destroy_all
    menu.update(name: attributes[:name], description: attributes[:description], price: attributes[:price])
    category_ids = attributes[:category_ids]
    categories = []
    category_ids.each do |category_id|
      categories << Category.find(category_id)
    end
    categories.each do |category|
      menu.menu_categories << MenuCategory.create(category: category)
    end
    menu
  end
end
