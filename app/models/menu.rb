class Menu < ApplicationRecord
  has_many :menu_categories
  has_many :categories, through: :menu_categories
  has_many :order_details

  validates :name, presence: true, uniqueness: {case_sensitive: false}
  validates :price, presence: true, numericality: true, comparison: { greater_than_or_equal_to: 0.01 }
  validates :description, length: {maximum: 150}

  validate :has_one_category_at_least

  def has_one_category_at_least
    if menu_categories.empty?
      errors.add(:menu_categories, "need one category at least")
    end
  end
end
