class Order < ApplicationRecord
  attr_accessor :order_details
  has_many :order_details, class_name: "OrderDetail"

  validates :email, presence: true,
            format: {with: /\A([^@\s]+)@((?:[-a-z0-9]+\.)+[a-z]{2,})\z/i, message: "invalid email"}

  accepts_nested_attributes_for :order_details, allow_destroy: true
end
