class Category < ApplicationRecord
  has_many :menu_categories
  has_many :menus, through: :menu_categories

  validates :name, presence: true, uniqueness: { case_sensitive: false },
            format: { with: /\A[^0-9]+\z/, message: "can't contain number"}
end
