# Gigih Catering

Lists, update, delete, create all available menu and categories for log the customers' orders

Still buggy, tried:
- soft delete -> wrong way, never mind
- nested resources for customers and orders -> can't recognize inputted value
- denormalize order table and customer table to simplify design -> can only add to orders, not the menu (current status)

note: ERD Schema is attached