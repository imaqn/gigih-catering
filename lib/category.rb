module CATEGORY
  def add_category(name)
    Category.create(name: name)
  end

  def add_relation(category)
    MenuCategory.create(category: category)
  end
end
