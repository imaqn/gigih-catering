class CreateOrders < ActiveRecord::Migration[7.0]
  def change
    create_table :orders do |t|
      t.decimal :total
      t.string :status
      t.string :email

      t.timestamps
    end
  end
end
