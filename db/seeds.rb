# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the bin/rails db:seed command (or created alongside the database with db:setup).
#
# Examples:
#
#   movies = Movie.create([{ name: "Star Wars" }, { name: "Lord of the Rings" }])
#   Character.create(name: "Luke", movie: movies.first)
categories = Category.create([{name: "Indonesian"}, {name: "Thailand"},
                              {name: "Japanese"}, {name: "Western"}, {name: "Asian"}, {name: "Main Dish"},
                              {name: "Appetizer"}, {name: "Dessert"}, {name: "Beverages"}])

menu1 = Menu.new(name: "Nasi goreng", description: "Nasi digoreng", price: 10000.0)
categories1 = [categories[0], categories[3], categories[4]]
categories1.each do |category|
  menu1.menu_categories << MenuCategory.create(category: category)
end
menu1.save

menu2 = Menu.new(name: "Dawet", description: "Minuman manis dengan cendol", price: 5000)
categories2 = [categories[0], categories[4], categories[8]]
categories2.each do |category|
  menu2.menu_categories << MenuCategory.create(category: category)
end
menu2.save
