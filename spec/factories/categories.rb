FactoryBot.define do
  factory :category do
    name { "Indonesian" }
  end

  factory :no_name_category, parent: :category do
    name { nil }
  end
end
