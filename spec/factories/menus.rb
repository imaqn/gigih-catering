FactoryBot.define do
  factory :menu do
    name { "Nasi uduk" }
    description { "Nasi dengan santan" }
    price { 10000.00 }

    factory :menu_with_category do
      categories { [association(:category)] }
    end
  end
end
