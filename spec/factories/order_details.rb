FactoryBot.define do
  factory :order_detail do
    quantity { 1 }
    price { "9.99" }
  end
end
