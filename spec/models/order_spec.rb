require 'rails_helper'

RSpec.describe Order, type: :model do
  it 'should belongs to Customer' do
    order = Order.reflect_on_association(:customer)
    expect(order.macro).to eq(:belongs_to)
  end
end
