require 'rails_helper'

RSpec.describe MenuCategory, type: :model do
  it 'should belongs to Menu' do
    menu_category = MenuCategory.reflect_on_association(:menu)
    expect(menu_category.macro).to eq(:belongs_to)
  end

  it 'should belongs to Category' do
    menu_category = MenuCategory.reflect_on_association(:category)
    expect(menu_category.macro).to eq(:belongs_to)
  end
end
