require 'rails_helper'

RSpec.describe OrderDetail, type: :model do
  it 'should belongs to Order' do
    order_detail = OrderDetail.reflect_on_association(:order)
    expect(order_detail.macro).to eq(:belongs_to)
  end

  it 'should belongs to Menu' do
    order_detail = MenuCategory.reflect_on_association(:menu)
    expect(order_detail.macro).to eq(:belongs_to)
  end
end
