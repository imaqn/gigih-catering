require 'rails_helper'

RSpec.describe Menu, type: :model do
  it 'should has many Category through MenuCategory' do
    menu = Menu.reflect_on_association(:menu_categories)
    expect(menu.macro).to eq(:has_many)
  end

  it 'should has at least one category' do
    menu = build(:menu)
    menu.valid?
    expect(menu.errors[:menu_categories]).to include("need one category at least")
  end

  it 'has a valid factory if associated with category' do
    menu = build(:menu_with_category)
    expect(menu).to be_valid
  end

  it 'can has more than one category' do
    menu = build(:menu)
    category1 = create(:category)
    category2 = create(:category, name: "Main dish")

    menu.categories << category1
    menu.categories << category2
    menu.save

    expect(Menu.find(1).menu_categories.pluck("category_id")).to eq([1, 2])
  end

  it 'is invalid without name' do
    menu = FactoryBot.build(:menu, name: nil)
    menu.valid?
    expect(menu.errors[:name]).to include("can't be blank")
  end

  it 'is invalid without price' do
    menu = FactoryBot.build(:menu, price: nil)
    menu.valid?
    expect(menu.errors[:price]).to include("can't be blank")
  end

  it 'is invalid with a duplicate name' do
    FactoryBot.create(:menu_with_category)
    menu2 = FactoryBot.build(:menu_with_category)

    menu2.valid?

    expect(menu2.errors[:name]).to include("has already been taken")
  end

  it 'price must be numerical' do
    menu = FactoryBot.build(:menu, price: "ten thousand")
    menu.valid?
    expect(menu.errors[:price]).to include("is not a number")
  end

  it 'price must be greater than or equal to 0.01' do
    menu = FactoryBot.build(:menu, price: 0.001)
    menu.valid?
    expect(menu.errors[:price]).to include("must be greater than or equal to 0.01")
  end

  it "description can't has more than 150 characters" do
    menu = FactoryBot.build(:menu, description: "Nasi dengan santan gurih tidak berkuah, \s
    \tsangat enak, sangat sedap, sangat mantap. Restriction of Right Colic Artery with\s
    \t Intraluminal Device, Percutaneous Approach")
    menu.valid?
    expect(menu.errors[:description]).to include("is too long (maximum is 150 characters)")
  end
end
