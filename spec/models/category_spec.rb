require 'rails_helper'

RSpec.describe Category, type: :model do
  it 'should has many Menu through MenuCategory' do
    category = Category.reflect_on_association(:menu_categories)
    expect(category.macro).to eq(:has_many)
  end

  it 'can accept more than one menu' do
    menu1 = build(:menu)
    menu2 = build(:menu, name: "soto")
    category = build(:category)

    menu1.categories << category
    menu1.save

    menu2.categories << category
    menu2.save

    expect(Category.find(1).menu_categories.pluck("menu_id")).to eq([1, 2])
  end

  it 'has valid factory' do
    category = FactoryBot.build(:category)
    expect(category). to be_valid
  end

  it "name can't be blank" do
    category = FactoryBot.build(:no_name_category)
    category.valid?
    expect(category.errors[:name]).to include("can't be blank")
  end

  it "name can't contain number" do
    category = build(:category, name: "123Indonesian")
    category.valid?
    expect(category.errors[:name]).to include("can't contain number")
  end

  it "can't has duplicate name" do
    FactoryBot.create(:category, name: "Main dish")
    category2 = FactoryBot.build(:category, name: "Main Dish")
    category2.valid?
    expect(category2.errors[:name]).to include("has already been taken")
  end
end
